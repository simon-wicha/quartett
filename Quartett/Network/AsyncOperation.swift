//
//  AsyncOperation.swift
//  Quartett
//
//  Created by Simon Wicha on 14/3/19.
//  Copyright © 2019 nomis-development. All rights reserved.
//

import Foundation
import UIKit

enum HTTPMethod: String {
    case GET = "GET"
    case POST = "POST"
}

class AsyncOperation: Operation {
    var responseData: Data?
    var error: Error?
    var httpMethod: HTTPMethod?
    
    private var backing_executing : Bool
    override var isExecuting : Bool {
        get { return backing_executing }
        set {
            willChangeValue(forKey: "isExecuting")
            backing_executing = newValue
            DispatchQueue.main.async {
                UIApplication.shared.isNetworkActivityIndicatorVisible = true
            }
            didChangeValue(forKey: "isExecuting")
        }
    }
    
    private var backing_finished : Bool
    override var isFinished : Bool {
        get { return backing_finished }
        set {
            willChangeValue(forKey: "isFinished")
            backing_finished = newValue
            DispatchQueue.main.async {
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
            }
            didChangeValue(forKey: "isFinished")
        }
    }
    
    override init() {
        backing_executing = false
        backing_finished = false
        
        super.init()
    }
    
    func completeOperation() {
        isExecuting = false
        isFinished = true
    }
}
