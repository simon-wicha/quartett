//
//  FetchQuartettCardOperation.swift
//  Quartett
//
//  Created by Simon Wicha on 14/3/19.
//  Copyright © 2019 nomis-development. All rights reserved.
//

import Foundation

class FetchQuartettCardOperation: AsyncOperation {
    
    private var url: String
    private var task: URLSessionDataTask?
    var responseJSONData: [String: Any]?
    var responseItem: Quartett?
    
    override init() {
        self.url = String(format: "https://poroba.com/tmp/tt_challenge.json")
        super.init()
        super.httpMethod = .GET
    }
    
    override func main() {
        if isCancelled {
            completeOperation()
            return
        }
        
        let session = URLSession(configuration: .ephemeral)
        var request = URLRequest(url: URL(string: url)!)
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        request.httpMethod = super.httpMethod.map { $0.rawValue }
        
        task = session.dataTask(with: request) { (jsonData, response, error) in
            guard let _jsonData = jsonData, let _response = response else {
                debugPrint("Failed Request from URL:\(error!.localizedDescription)")
                super.error = error
                self.completeOperation()
                return
            }
            do {
                super.responseData = _jsonData
                
                self.responseJSONData = try JSONSerialization.jsonObject(with: _jsonData, options: []) as? [String: Any]
                print("Successful Response from URL: \(String(describing: _response.url!.absoluteString))")
                let quartett = try JSONDecoder().decode(Quartett.self, from: _jsonData)
                self.responseItem = quartett
                self.completeOperation()
            } catch let error {
                debugPrint("Error serializing jsonObject (\(String(describing: _response.url!.absoluteString))): \(error.localizedDescription)")
                super.error = error
                self.completeOperation()
                return
            }
        }
        task?.resume()
    }
}

