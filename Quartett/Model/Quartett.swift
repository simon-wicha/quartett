//
//  Quartett.swift
//  Quartett
//
//  Created by Simon Wicha on 14/3/19.
//  Copyright © 2019 nomis-development. All rights reserved.
//

import Foundation

struct Quartett: Codable {
    let manufacturer: String?
    let model: String?
    let year: Int?
    let engine: Engine?
    let consumption: String?
    let emission: Double?
    let topSpeed: Int?
    let price: Int?
    let seats: Int?
    let picture: String?
    
    enum CodingKeys: String, CodingKey {
        case manufacturer
        case model
        case year
        case engine
        case consumption = "consumptionLKm"
        case emission = "emissionGKm"
        case topSpeed = "topSpeedKMh"
        case price = "priceEuro"
        case seats
        case picture
    }
}

struct Engine: Codable {
    let cylinders: String?
    let displacement: Double?
    let power: Int?
    let torque: Int?
    
    enum CodingKeys: String, CodingKey {
        case cylinders
        case displacement = "displacementL"
        case power = "powerKWh"
        case torque = "torqueNM"
    }
}
