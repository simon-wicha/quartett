//
//  ViewController.swift
//  Quartett
//
//  Created by Simon Wicha on 14/3/19.
//  Copyright © 2019 nomis-development. All rights reserved.
//

import UIKit
import SDWebImage

class CarViewController: UIViewController {

    @IBOutlet weak var topTitleLabel: UILabel!
    @IBOutlet weak var carImageView: UIImageView!
    @IBOutlet weak var displacementCylinderLabel: UILabel!
    @IBOutlet weak var powerLabel: UILabel!
    @IBOutlet weak var torqueLabel: UILabel!
    @IBOutlet weak var maxSpeedLabel: UILabel!
    @IBOutlet weak var consumptionLabel: UILabel!
    @IBOutlet weak var seatsLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    
    @IBOutlet weak var detailsStackView: UIStackView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Quartett"
        hideViews(true)
        let operation = FetchQuartettCardOperation()
        operation.completionBlock = {
            DispatchQueue.main.async {
                if let quartett = operation.responseItem, operation.error == nil {
                    self.hideViews(false)
                    self.configureUI(quartett)
                } else {
                    //TODO: Error
                }
            }
        }
        operation.start()
    }
    
    func hideViews(_ hide: Bool) {
        topTitleLabel.isHidden = hide
        carImageView.isHidden = hide
        detailsStackView.isHidden = hide
    }

    func configureUI(_ quartett: Quartett) {
        topTitleLabel.text = String(format: "%@ %@", quartett.manufacturer ?? "", quartett.model ?? "")
        if let imageURL = quartett.picture {
            carImageView.sd_setImage(with: URL(string: imageURL), placeholderImage: UIImage(named: "no_image"), options: .highPriority, completed: nil)
        } else {
            carImageView.image = UIImage(named: "no_image")
        }
        displacementCylinderLabel.text = String(format: "%.1f Liter / %@", quartett.engine?.displacement ?? "", quartett.engine?.cylinders ?? "")
        
        placeInfo(quartett.engine?.power, "KWh", powerLabel)
        placeInfo(quartett.engine?.torque, "Nm", torqueLabel)
        placeInfo(quartett.topSpeed, "KMh", maxSpeedLabel)
        placeInfo(quartett.consumption, "LKm", consumptionLabel)
        placeInfo(quartett.seats, "", seatsLabel)
        placeInfo(quartett.price, "€", priceLabel)
    }
    
    func placeInfo(_ value: Any?, _ unit: String, _ label: UILabel) {
        if let _value = value {
            label.text = "\(_value) \(unit)"
        } else {
            label.text = "N/A"
        }
    }
}

